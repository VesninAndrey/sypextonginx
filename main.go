package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sypextonginx/sypex"
)

func main() {
	geo := sypexgeo.New("SxGeoCity.dat")

	var startIp = [4]int{}
	var endIp = [4]int{}
	var prevISO string = ""
	var currentISO string = ""
	var currentIP string
	var countryISO string

	f, _ := os.Create("geo.conf")
	w := bufio.NewWriter(f)
	defer w.Flush()

	var ip4 int
	for ip1 := 0; ip1 <= 255; ip1++ {
		for ip2 := 0; ip2 <= 255; ip2++ {
			for ip3 := 0; ip3 <= 255; ip3++ {
				currentIP = fmt.Sprintf("%d.%d.%d.%d", ip1, ip2, ip3, ip4)
				currentISO = geo.GetISO(currentIP)

				if currentISO != prevISO {
					if prevISO != "" {
						if len(prevISO) < 2 {
							log.Println(prevISO)
						}
						countryISO = prevISO[:2]
						//Россия
						//Украина
						//Казахстан
						//Белоруссия
						if countryISO == "RU" || countryISO == "UA" || countryISO == "KZ" || countryISO == "BY" {
							w.WriteString(fmt.Sprintf("%d.%d.%d.%d-%d.%d.%d.%d %s;\n", startIp[0], startIp[1], startIp[2], startIp[3], endIp[0], endIp[1], endIp[2], endIp[3], prevISO))
							log.Println(fmt.Sprintf("%d.%d.%d.%d-%d.%d.%d.%d %s;", startIp[0], startIp[1], startIp[2], startIp[3], endIp[0], endIp[1], endIp[2], endIp[3], prevISO))
						}
					}

					prevISO = currentISO
					startIp = [4]int{ip1, ip2, ip3, ip4}
				}

				if prevISO != "" && currentISO != "" {
					endIp = [4]int{ip1, ip2, ip3, ip4}
				}
			}
			log.Println("IP", ip1, ip2)
		}
	}
}
